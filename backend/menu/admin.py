from django.contrib import admin
from menu.models import Title, Benefit, Nav


@admin.register(Title)
class TitleAdmin(admin.ModelAdmin):
    list_display = ('title', 'subtitle')


@admin.register(Benefit)
class BenefitAdmin(admin.ModelAdmin):
    list_display = ('up_title', 'content', 'down_title')


@admin.register(Nav)
class NavAdmin(admin.ModelAdmin):
    list_display = ('title', 'path')