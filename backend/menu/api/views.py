from rest_framework import viewsets
from menu.api.serializers import TitleSerializer, BenefitSerializer, NavSerializer
from menu.models import Title, Benefit, Nav


class TitleViewSet(viewsets.ModelViewSet):
    queryset = Title.objects.all()
    serializer_class = TitleSerializer


class BenefitViewSet(viewsets.ModelViewSet):
    queryset = Benefit.objects.filter(is_available=True)
    serializer_class = BenefitSerializer


class NavViewSet(viewsets.ModelViewSet):
    queryset = Nav.objects.filter(is_available=True)
    serializer_class = NavSerializer