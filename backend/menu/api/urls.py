from menu.api.views import TitleViewSet, BenefitViewSet, NavViewSet
from rest_framework.routers import DefaultRouter

app_name = 'menu_api'

router = DefaultRouter()
router.register(r'title', TitleViewSet, basename='title')
router.register(r'benefit', BenefitViewSet, basename='benefit')
router.register(r'nav', NavViewSet, basename='nav')

urlpatterns = router.urls