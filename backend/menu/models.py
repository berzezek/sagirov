from django.db import models


class Title(models.Model):
    title = models.CharField(verbose_name='Заголовок', max_length=100)
    subtitle = models.CharField(verbose_name='Подзаголовок', max_length=255)

    class Meta:
        verbose_name = 'Заголовок'
        verbose_name_plural = 'Заголовок'
        ordering = ('-id',)

    def __str__(self):
        return self.title


class Benefit(models.Model):
    up_title = models.CharField(verbose_name='Верхний заголовок', max_length=20)
    content = models.CharField(verbose_name='Содержимое', max_length=5)
    down_title = models.CharField(verbose_name='Нижний заголовок', max_length=20)
    is_available = models.BooleanField(verbose_name='Показывать на странице', default=True)

    class Meta:
        verbose_name = 'Преимущество'
        verbose_name_plural = 'Преимущества'

    def __str__(self):
        return self.up_title


class Nav(models.Model):
    title = models.CharField(verbose_name='Наименование страницы', max_length=100)
    path = models.CharField(verbose_name='Путь', max_length=255)
    is_available = models.BooleanField(verbose_name='Показывать на странице', default=True)

    class Meta:
        verbose_name = 'Навигация'
        verbose_name_plural = 'Навигация'

    def __str__(self):
        return self.title