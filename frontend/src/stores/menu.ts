import { defineStore } from 'pinia'


const BASE_API_URL = 'http://127.0.0.1:8000/api/v1/';

export const useMenuStore = defineStore('menu', {
  state: () => ({
    items: [],
    item: {},
    error: null as any,
    isFetching: false,
  }),
  actions: {
    async fetchItems(url: string) {
      this.isFetching = true
      try {
        const response = await fetch(`${BASE_API_URL}${url}/`)
        this.items = await response.json();
        return this.items
      } catch (error) {
        this.error = error
      } finally {
        this.isFetching = false
      }
    },
  },
})