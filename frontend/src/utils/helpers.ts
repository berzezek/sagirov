// Функия для изменения первой буквы заголовка на заглавную
export const myCapitalize = (str: string): string => {
    return str.charAt(0).toUpperCase() + str.slice(1)
  }